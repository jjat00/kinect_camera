"""
  Archivo: pptklib.py
  Autor: Jaimen Aza <userjjat00@correounivalle.edu.co>
  Fecha creación: 2019-06-06
  Licencia: GNU-GPL
  Funcionalidad: Graficar nube de puntos capturada por el sensor kinect
"""

import numpy as np
import pptk
import time
import PointCloud

#Obtiene los puntos en las coordenadas x,y,z
Pxyz = PointCloud.getPointCloud(1)
scalars = np.arange(Pxyz[0].size//3)

#Visualizar puntos 
v = pptk.viewer(Pxyz, scalars)

#Configura la visualización (color fondo, sin grid, color puntos)
v.set(bg_color=[0,0,0,1],show_grid = False)
v.color_map('cool', scale=[0, 5])
v.color_map([[0, 1, 0.5], [1,0,1]])