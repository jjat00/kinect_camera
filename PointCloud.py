"""
  Archivo: PointCloud.py
  Autor: Jaimen Aza <userjjat00@correounivalle.edu.co>
  Fecha creación: 2019-06-06
  Licencia: GNU-GPL
"""


#Obtiene las coordenadas (x,y,z) de cada punto, y los vectores u,v. Recibe  como parametros los N numeros 
def getPointCloud(NoCloud):
  #librerias necesarias en la funcion getPoints
  import calibkinect  
  import time
  import freenect
  import numpy as np

  u,v = np.mgrid[:480:2,:640:2]
  P2xyz = []

  for i in range(NoCloud):
    Pxyz0,uv = calibkinect.depth2xyzuv(freenect.sync_get_depth()[0][::2,::2], u, v)
    P2xyz.append(Pxyz0)
  return np.asarray(P2xyz)


#Dibuja cada punto en su respectiva coordenada
def drawPointCloud(Pxyz):
  #librerias necesarias en la funcion drawPointCloud
  import numpy as np
  from glumpy import app, gloo, gl
  from glumpy.transforms import TrackballPan, Position

  vertex = """
  attribute vec3 position;
    void main() {
        gl_PointSize = 1.0;
        gl_Position = <transform>;
    } """
  fragment = """
    void main() {
         gl_FragColor = vec4(1.0,0.1,0,1.0);

    } """
  fragment1 = """
    void main(){
         gl_FragColor = vec4(0.0,0.2,1,1.0);

    } """
 

  window = app.Window(1300,1000, color=(0.0,0.0,0.0,1))

  #Build point cloud data
  points = gloo.Program(vertex, fragment, count=Pxyz[0].size//3)
  points["position"] = Pxyz[0]

  # create an instance of the TrackballPan object.
  trackball = TrackballPan(Position("position"), znear=5, zfar=10, distance=5)
  points['transform'] = trackball
  trackball.aspect = 1
  # rotation around the X axis
  trackball.phi = 90
  # rotation around the Y axis
  trackball.theta = 0
  trackball.zoom = 20

  points1 = gloo.Program(vertex, fragment1, count=Pxyz[1].size//3)
  points1["position"] = Pxyz[1]

  # create an instance of the TrackballPan object.
  trackball = TrackballPan(Position("position"), znear=5, zfar=10, distance=5)
  points1['transform'] = trackball
  trackball.aspect = 1
  # rotation around the X axis
  trackball.phi = 90
  # rotation around the Y axis
  trackball.theta = 0
  trackball.zoom = 15

  @window.event
  def on_draw(dt):
      window.clear()
      points.draw(gl.GL_POINTS)
      points1.draw(gl.GL_POINTS)
      #window.close()
       
  window.attach(points['transform'])
  window.attach(points1['transform'])
  app.run()


if __name__=='__main__':
    for i in range(1):
      drawPointCloud(getPointCloud(2))